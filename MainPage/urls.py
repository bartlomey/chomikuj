
from django.conf.urls import url
from django.contrib import admin

from MainPage import views

app_name = 'MainPage'

urlpatterns = \
[
    url(r'^admin/', admin.site.urls),
    url(r'^register/$', views.register, name='register'),
    url(r'^login/$', views.login_user, name='login'),
    url(r'^$', views.login_user, name='login'),
    url(r'^editprofile/$', views.edit_profile, name='editprofile'),
    url(r'^logout/$', views.logout_user, name='logout_user'),
    url(r'^home/$', views.home, name='home'),
    url(r'^showprofiles/(?P<profile_id>[0-9]+)/$', views.show_profile, name='show_profile'),
    url(r'^showprofiles/$', views.show_profile_list, name='show_profile_list'),
    url(r'^settings/$', views.settings, name='settings'),
    url(r'^follows/$', views.follows, name='follows'),
    url(r'^files/$', views.manage_files, name='manage_files'),
    url(r'^comments/$', views.manage_comments, name='manage_comments')

]

