import os

from django.http import HttpResponseRedirect
from django.shortcuts import render, get_object_or_404
from django.contrib.auth import logout
from django.contrib.auth import authenticate, login
from django.urls import reverse
from .models import Profile, Posts, Relationship, Files
from .forms import UserForm, ProfileForm, TimelineForm
from celery.schedules import crontab
from celery.task import periodic_task


@periodic_task(run_every=crontab(hour=21, minute=43, day_of_week=3))
def every_monday_morning():
    profiles = Profile.objects.all()
    for profile in profiles:
        profile.used_transfer = 0
        profile.save()


def logout_user(request):
    logout(request)
    form = UserForm(request.POST or None)
    return render(request, 'MainPage/login.html', {"form": form})


def register(request):
    form = UserForm(request.POST or None)
    if form.is_valid():
        user = form.save(commit=False)
        username = form.cleaned_data['username']
        password = form.cleaned_data['password']
        user.set_password(password)
        user.save()
        user = authenticate(username=username, password=password)
        if user is not None:
            if user.is_active:
                login(request, user)
                url = reverse('MainPage:editprofile', args=(),
                              kwargs={})
                return HttpResponseRedirect(url)
    context = {
        "form": form,
    }
    return render(request, 'mainpage/register.html', context)


def login_user(request):
    if request.method == "POST":
        username = request.POST['username']
        password = request.POST['password']
        user = authenticate(username=username, password=password)

        if user is not None:
            if user.is_active:
                login(request, user)
                try:
                    get_object_or_404(Profile, auth=request.user)
                except:
                    url = reverse('MainPage:editprofile', args=(),
                                  kwargs={})
                    return HttpResponseRedirect(url)
                url = reverse('MainPage:home', args=(),
                              kwargs={})
                return HttpResponseRedirect(url)
            else:
                return render(request, 'MainPage/login.html',
                              {'error_message': 'Konto nieaktywne. Skontaktuj się z adminem.'})
        else:
            return render(request, 'MainPage/login.html', {'error_message': 'Invalid login'})
    return render(request, 'MainPage/login.html')


def edit_profile(request):
    try:
        profile = Profile.objects.get(auth=request.user)
        url = reverse('MainPage:home', args=(),
                      kwargs={})
        return HttpResponseRedirect(url)
    except:
        print('exception')

    if request.method == 'POST':
        form = ProfileForm(request.POST or None, request.FILES or None)
        if form.is_valid():
            profile = form.save(commit=False)
            profile.auth = request.user
            profile.first_name = form.cleaned_data['first_name']
            profile.last_name = form.cleaned_data['last_name']
            profile.photo = request.FILES['photo']
            profile.age = form.cleaned_data['age']
            profile.city = form.cleaned_data['city']
            profile.email = form.cleaned_data['email']
            profile.save()
            url = reverse('MainPage:home', args=(),
                          kwargs={})
            return HttpResponseRedirect(url)

        error = {
            "error_message": "Uzupełnij wszystkie pola!",
        }
        return render(request, 'mainpage/editprofile.html', error)
    return render(request, 'mainpage/editprofile.html')


def home(request):
    if not request.user.is_authenticated():
        return render(request, "MainPage/login.html")
    if request.user is not None:
        if request.user.is_active:
            try:
                get_object_or_404(Profile, auth=request.user)
            except:
                url = reverse('MainPage:editprofile', args=(),
                              kwargs={})
                return HttpResponseRedirect(url)
    if request.method == 'POST':
        form = TimelineForm(request.POST or None)
        if form.is_valid():
            post = form.save(commit=False)
            post.profile = Profile.objects.get(auth=request.user)
            post.timeline_id = request.user.id
            post.text = form.cleaned_data['text']
            post.save()

    if request.method == 'POST' and not TimelineForm(request.POST or None).is_valid():
        requestFile = request.FILES['file']
        profile = get_object_or_404(Profile, auth=request.user)
        filesSize = profile.used_transfer
        if filesSize + requestFile.size >= 2000000000:
            profile = get_object_or_404(Profile, auth=request.user)
            timeline = Posts.objects.filter(timeline_id=request.user.id)
            files = Files.objects.filter(user=profile)
            return render(request, 'MainPage/home2.html', {'profile': profile, 'timeline': timeline, 'files': files,
                                                           'message': "Nie masz miejsca na dysku!"})
        files = Files()
        files.file = requestFile
        files.name = requestFile.name
        files.size = requestFile.size
        files.user = get_object_or_404(Profile, auth=request.user)
        profile.used_transfer += files.size
        profile.save()
        files.save()

    profile = get_object_or_404(Profile, auth=request.user)
    timeline = Posts.objects.filter(timeline_id=request.user.id)
    files = Files.objects.filter(user=profile)

    return render(request, 'MainPage/home2.html', {'profile': profile, 'timeline': timeline, 'files': files})


def show_profile(request, profile_id):
    if not request.user.is_authenticated():
        return render(request, "MainPage/login.html")
    if request.method == 'POST':
        form = TimelineForm(request.POST or None)
        if form.is_valid():
            post = form.save(commit=False)
            post.profile = Profile.objects.get(auth=request.user)
            post.timeline_id = profile_id
            post.text = form.cleaned_data['text']
            post.save()
    profile = Profile.objects.get(auth_id=profile_id)
    timeline = Posts.objects.filter(timeline_id=profile_id)
    files = Files.objects.filter(user=profile)
    return render(request, 'MainPage/profile.html', {'profile': profile, 'timeline': timeline, 'files': files})


def settings(request):
    if not request.user.is_authenticated():
        return render(request, "MainPage/login.html")
    if request.method == 'POST':
        try:
            first_name = request.POST['first_name']
        except:
            first_name = None
        try:
            last_name = request.POST['last_name']
        except:
            last_name = None
        try:
            photo = request.FILES['photo']
        except:
            photo = None
        try:
            age = request.POST['age']
        except:
            age = None
        try:
            email = request.POST['email']
        except:
            email = None
        try:
            city = request.POST['city']
        except:
            city = None
        profile = Profile.objects.get(auth=request.user)
        if first_name != "":
            profile.first_name = first_name
            profile.save()
        if last_name != "":
            profile.last_name = last_name
            profile.save()
        if photo is not None:
            profile.photo = photo
            profile.save()
        if age != "":
            profile.age = age
            profile.save()
        if email != "":
            profile.email = email
            profile.save()
        if city != "":
            profile.city = city
            profile.save()

        profile.save()
        url = reverse('MainPage:home', args=(),
                      kwargs={})
        return HttpResponseRedirect(url)

    return render(request, "MainPage/settings.html")


def show_profile_list(request):
    if not request.user.is_authenticated():
        return render(request, "MainPage/login.html")
    profiles = Profile.objects.all()
    if request.method == 'POST':
        query = request.POST.get('q')
        if query:
            query_list = query.split()
            if len(query_list)==2:
                profiles = Profile.objects.filter(first_name=query_list[0],last_name=query_list[1])
                return render(request, 'MainPage/users.html', {'profiles': profiles})
        if request.POST.get('button'):
            user_id = request.POST.get('button')
            profile1 = Profile.objects.get(auth=request.user)
            profile2 = Profile.objects.get(id=user_id)
            relationship = Relationship(user1_id=profile1, user2_id=profile2)
            relationship.save()
            return render(request, 'MainPage/users.html', {'profiles': profiles, 'user': user_id})
    return render(request, 'MainPage/users.html', {'profiles': profiles})


def follows(request):
    if not request.user.is_authenticated():
        return render(request, "MainPage/login.html")
    if request.method == 'POST':
        user_id = request.POST.get('button')
        profile1 = Profile.objects.get(auth=request.user)
        profile2 = Profile.objects.get(id=user_id)
        relationship = Relationship.objects.get(user1_id=profile1, user2_id=profile2)
        relationship.delete()
    my_profile = Profile.objects.get(auth=request.user)
    relationships = Relationship.objects.all()
    profiles = []
    for relationship in relationships:
        if relationship.user1_id == my_profile:
            profiles.append(relationship.user2_id)
    return render(request, 'MainPage/follows.html', {'profiles': profiles})


def manage_files(request):
    if not request.user.is_authenticated():
        return render(request, "MainPage/login.html")
    profile = Profile.objects.get(auth=request.user)
    if request.method == 'POST':
        query = request.POST.get('q')
        if query:
            files = Files.objects.filter(name=query, user_id=Profile.objects.get(auth=request.user))
            files2 = Files.objects.filter(user=profile)
            filesSize = sum([file.size for file in files2])
            usedStorage = round(filesSize / 2000000000 * 100, 2)
            return render(request, 'MainPage/manage_files.html', {'files': files, 'usedStorage': usedStorage})
        file_id = request.POST.get('button')
        file = Files.objects.get(id=file_id)
        profile.used_transfer -= file.size
        profile.save()
        os.remove(file.file.path)
        file.delete()
    files = Files.objects.filter(user=profile)
    filesSize = sum([file.size for file in files])
    usedStorage = round(filesSize / 2000000000 * 100, 2)

    return render(request, 'MainPage/manage_files.html', {'files': files, 'usedStorage': usedStorage})


def manage_comments(request):
    if request.method == 'POST':
        post_id = request.POST.get('button')
        post = Posts.objects.get(id=post_id)
        post.delete()

    timeline = Posts.objects.filter(timeline_id=request.user.id)
    return render(request, 'MainPage/manage_comments.html', {'timeline': timeline})

