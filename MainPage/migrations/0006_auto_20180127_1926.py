# -*- coding: utf-8 -*-
# Generated by Django 1.10.6 on 2018-01-27 18:26
from __future__ import unicode_literals

import MainPage.models
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('MainPage', '0005_files_name'),
    ]

    operations = [
        migrations.AlterField(
            model_name='files',
            name='file',
            field=models.FileField(upload_to=MainPage.models._upload_path),
        ),
        migrations.AlterField(
            model_name='files',
            name='name',
            field=models.TextField(default='name'),
        ),
    ]
