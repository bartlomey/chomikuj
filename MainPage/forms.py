from django import forms
from django.contrib.auth.models import User
from .models import Profile, Posts


class UserForm(forms.ModelForm):
    password = forms.CharField(widget=forms.PasswordInput)

    class Meta:
        model = User
        fields = ['username', 'password']


class ProfileForm(forms.ModelForm):
    first_name = forms.CharField()
    last_name = forms.CharField()

    class Meta:
        model = Profile
        fields = ['first_name', 'last_name', 'age', 'city', 'photo','email']


class TimelineForm(forms.ModelForm):
    class Meta:
        model = Posts
        fields = ['text']

