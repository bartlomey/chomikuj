from django.core.files.storage import FileSystemStorage
from django.db import models
from django.contrib.auth.models import User
from datetime import date

fs = FileSystemStorage(location='/media/files')


def _upload_path(instance, filename):
    return instance.get_upload_path(filename)


class Profile(models.Model):
    id = models.IntegerField(primary_key=True)
    auth = models.ForeignKey(User, null=True)
    first_name = models.CharField(max_length=128, default="first name")
    last_name = models.CharField(max_length=128, default="last name")
    age = models.PositiveIntegerField(null=True)
    city = models.CharField(max_length=128, null=True)
    photo = models.ImageField(null=True)
    email = models.CharField(max_length=1024, default='email')
    used_transfer = models.FloatField(default=0)

    def __str__(self):
        return str(self.first_name + " " + self.last_name)


class Relationship(models.Model):
    class Meta:
        unique_together = ('user1_id', 'user2_id')

    user1_id = models.ForeignKey(Profile, related_name="user_one")
    user2_id = models.ForeignKey(Profile, related_name="user_two")

    def __str__(self):
        return self.id


class Posts(models.Model):
    profile = models.ForeignKey(Profile, null=True, related_name="profile")
    timeline_id = models.IntegerField(default=0)
    text = models.CharField(max_length=3000)
    class Meta:
        verbose_name_plural = "Posts"


class Files(models.Model):
    file = models.FileField(upload_to=_upload_path)
    name = models.TextField(default="name")
    user = models.ForeignKey(Profile, related_name="user")
    date = models.DateField(default=date.today)
    size = models.FloatField(default=0)

    class Meta:
        verbose_name_plural = "Files"

    def get_upload_path(self, filename):
        return "static/uploads/" + str(self.user.id) + "/" + filename
