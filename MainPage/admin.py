from django.contrib import admin

from .models import *
from .forms import UserForm

# Register your models here.

admin.site.register([Posts, Profile, Files, Relationship])